declare variable $project external;
declare variable $id external;

<div class="tei2pdf">
    <a role="button" title="Export pdf" target="_blank" href="/{$project}/{$id}.pdf">
        &#8682;<span class="texte">PDF</span>
    </a>
</div>

